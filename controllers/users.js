var User = require("../models/User");

module.exports.controller = (app) => {
    // get all users
    app.get('/users', (req, res) => {
    User.find({}, 'name email address', function (error, users) {
        if (error) { console.log(error); }
            res.send(users);
        })
    });

    //get a single user details
    app.get('/users/:id', (req, res) => {
        User.findById(req.params.id, 'name email address', function (error, user) {
            if (error) { console.log(error); }
            res.send(user)
        })
    });
}
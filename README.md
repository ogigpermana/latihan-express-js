## Latihan Membuat Aplikasi Express JS ##

Untuk mencoba aplikasi ini caranya mudah sekali
-   Silahkan di clone aplikasi ini
-   Pastikan node.js sudah terinstall di mesin komputer
-   Masuk ke Folder Aplikasi yang sudah di clone lewat terminal
-   Ketikan ``npm install`` untuk meng install modules yang dibutuhkan
-   Untuk menjalankannya ketikan ``node app.js``
-   Untuk menjalankan menggunakan nodemon ketikan ``nodemon app.js`` aplikasi akan reload otomatis apabila da perubahan pada file yang sedang di develop
-   Buka browser kemudian ketikan ``localhost:3000``
